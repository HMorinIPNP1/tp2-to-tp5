import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

t = np.arange(-1.0, 5.0, 0.01)
t1=1
t2=2
s=[]
g=np.exp(-1*t)
for i in range(len(t)):
	if t[i]<t1:
		s.append(0)
	if (t[i]>=t1) and (t[i]<=t2):
		s.append(1)
	if t[i]>t2:
		s.append(0)
corr = signal.correlate(s, g)
conv = signal.convolve(s, g)
fig, ax = plt.subplots(4)
ax[0].plot(t,s)
ax[0].set(title='f(t)')
ax[1].plot(t,g)
ax[1].set(title='g(t)')
ax[2].plot(corr)
ax[2].set(title='correlation')
ax[3].plot(conv)
ax[3].set(title='convolution')
plt.show()
fig.savefig("Ex3.png")
input()