import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

slt1=[1000, 100, 10]
slt2=[0.0001, 0.01, 1]

sig = signal.lti(slt1, slt2)
x1, y1 = signal.step(sig)
x2, y2 = signal.impulse(sig)

fig, ax = plt.subplots(2)
ax[0].plot(x1,y1)
ax[1].plot(x2, y2)
ax[0].set(title='step')
ax[1].set(title='impulse')
plt.show()
fig.savefig("Ex4a.png")
input()