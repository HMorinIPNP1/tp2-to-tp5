import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0.0, 10.0, 0.01)
t1=1
t2=2
s=[]

g=np.exp(-1*-t)

s2=(1-np.exp(-1*(t-t1)))
s3=np.exp(-1*t)*(np.exp(t2)-np.exp(t1))

g2=(1-np.exp(-1*(t+t1)))
g3=np.exp(-1*-t)*(np.exp(t2)-np.exp(t1))
conv=[]
corr=[]
for i in range(len(t)):
	if t[i]<t1:
		s.append(0)
		conv.append(0)
		corr.append(0)
	if (t[i]>=t1) and (t[i]<=t2):
		s.append(1)
		conv.append(s2[i])
		corr.append(g2[i])
	if t[i]>t2:
		s.append(0)
		conv.append(s3[i])
		corr.append(g3[i])

fig, ax = plt.subplots(3)
ax[0].plot(t,s)
ax[1].plot(conv)
ax[2].plot(corr)
ax[0].set(title='f(t)')
ax[1].set(title='convolution')
ax[2].set(title='correlation')
plt.show()
fig.savefig("Ex1.png")
input()