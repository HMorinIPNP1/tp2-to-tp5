import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

slt1=[]
slt2=[]
for i in range(0,4):
	slt1.append(np.exp(-1*i))

for j in range(0,6):
	slt2.append(np.exp(-1*j))

sig = signal.lti(slt1, slt2)
x1, y1 = signal.step(sig)
x2, y2 = signal.impulse(sig)

fig, ax = plt.subplots(2)
ax[0].plot(x1,y1)
ax[1].plot(x2, y2)
ax[0].set(title='step')
ax[1].set(title='impulse')
plt.show()
fig.savefig("Ex4b.png")
input()