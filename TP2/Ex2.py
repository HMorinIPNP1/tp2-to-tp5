import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-1.0, 5.0, 0.01)
t1=100
t2=200
s=[]
g=np.exp(-1*t)
for i in range(len(t)):
	if i<t1:
		s.append(0)
	if (i>=t1) and (i<=t2):
		s.append(1)
	if i>t2:
		s.append(0)

fig, ax = plt.subplots(2)
ax[0].plot(t,s)
ax[0].set(title='f(t)')
ax[1].plot(t,g)
ax[1].set(title='g(t)')
plt.show()
fig.savefig("Ex2.png")
input()