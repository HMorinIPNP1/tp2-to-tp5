import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

t = np.arange(-2, 2, 0.001)
f = 5
s = (np.sin(np.pi * f * t)) / (np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)
ax.set(title='cheby1')
fig.savefig("Ex6.png")
plt.show()
