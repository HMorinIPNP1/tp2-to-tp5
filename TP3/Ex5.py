import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

t = np.arange(0, 1, 0.001)
f1 = 5
f2 = 30
s1 = np.sin(2*np.pi*f1*t)
s2 = np.sin(2*np.pi*f2*t)
sig = s1+s2

sos = signal.cheby1(25, 1, 15, 'hp', fs=2000, output='sos')
filtcheby1 = signal.sosfilt(sos, sig)
sos = signal.cheby2(25, 50, 15, 'hp', fs=2000, output='sos')
filtcheby2 = signal.sosfilt(sos, sig)
sos = signal.butter(25, 15, 'hp', fs=2000, output='sos')
filtbutter = signal.sosfilt(sos, sig)
sos = signal.ellip(25, 1, 50, 15, 'hp', fs=2000, output='sos')
filtcauer = signal.sosfilt(sos, sig)
fig, ax = plt.subplots(1)
ax.plot(t, filtcheby1,label='cheby1')
ax.plot(t, filtcheby2,label='cheby2')
ax.plot(t, filtbutter,label='butter')
ax.plot(t, filtcauer,label='cauer')
ax.axis([0, 0.15, -2, 2])
plt.legend(framealpha=1, frameon=True)
fig.savefig("Ex5.png")
plt.show()
