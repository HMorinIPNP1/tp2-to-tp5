import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

t = np.arange(-2, 2, 0.001)
f = 5
s = (np.sin(np.pi * f * t)) / (np.pi * t)
fe1 = 5
fe2 = 10
fe3 = 50
t1 = np.arange(-2, 2, 1/fe1)
t2 = np.arange(-2, 2, 1/fe2)
t3 = np.arange(-2, 2, 1/fe3)
s1 = (np.sin(np.pi * f * t1)) / (np.pi * t1)
s2 = (np.sin(np.pi * f * t2)) / (np.pi * t2)
s3 = (np.sin(np.pi * f * t3)) / (np.pi * t3)
fig, ax = plt.subplots(4)

ax[0].plot(t, s)
ax[0].set(title='Signal')
ax[0].axis([-2, 2, -2, 6])
ax[1].plot(t1, s1)
ax[1].set(title='Echantillonnage 1')
ax[1].axis([-2, 2, -2, 6])
ax[2].plot(t2, s2)
ax[2].set(title='Echantillonnage 2')
ax[2].axis([-2, 2, -2, 6])
ax[3].plot(t3, s3)
ax[3].set(title='Echantillonnage 3')
ax[3].axis([-2, 2, -2, 6])
fig.savefig("Ex7.png")
plt.show()
