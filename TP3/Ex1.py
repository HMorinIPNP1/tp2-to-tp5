import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

t = np.arange(0, 1, 0.001)
f1 = 5
f2 = 30
s1 = np.sin(2*np.pi*f1*t)
s2 = np.sin(2*np.pi*f2*t)
sig = s1+s2
#fourier = np.fft.fft(s1)
#freq = np.fft.fftfreq(s1.size, 0.001)
#fourier2 = np.fft.fft(s2)
#freq = np.fft.fftfreq(s1.size, 0.001)

sos = signal.cheby1(25, 1, 15, 'hp', fs=2000, output='sos')
filtered = signal.sosfilt(sos, sig)
fig, ax = plt.subplots(2)
#fig, ax = plt.subplots(4)
ax[0].plot(t, sig)
ax[0].axis([0, 1, -2, 2])
ax[1].plot(t, filtered)
ax[1].axis([0, 1, -2, 2])
#ax[2].plot(freq, fourier)
#ax[3].plot(freq, fourier2)
fig.savefig("Ex1.png")
plt.show()
