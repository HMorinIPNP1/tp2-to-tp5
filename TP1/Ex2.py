import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-2.0, 2.0, 0.01)
s = 1 *(abs(t) <=1/2 )

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)', title='About as simple as it gets, folks')
ax.grid()

fig.savefig("Ex2.png")
plt.show()
