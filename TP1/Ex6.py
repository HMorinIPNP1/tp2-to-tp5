import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.linspace(-0.02, 0.05, 1000)
s = np.exp(2j*100*np.pi*t).real

fig, ax = plt.subplots()
ax.plot(t, s, label="real")
ax.legend()
plt.show()
fig.savefig("Ex6.png")
