import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(-5.0, 5.0, 0.01)
s = np.exp(-1*abs(t))

fourier = np.fft.fft(s)
n = s.size
freq = np.fft.fftfreq(n, 0.01)
xf = np.fft.fftshift(freq) /100
appro = np.fft.fftshift(fourier)/100

fig, ax = plt.subplots(2)
ax[0].plot(t,s, label="signal")
ax[1].plot(xf,np.abs(appro), label="shift")
ax[0].legend()
ax[1].legend()
plt.show()
fig.savefig("Ex10.png")
