import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0.0, 2.0, 0.01)
s = np.sin(t)
c = np.cos(t)
h = np.sin(t) + np.cos(t)

fig, ax = plt.subplots(3)
ax[0].plot(t, s)
ax[1].plot(t, c)
ax[2].plot(t, h)

ax[0].set(xlabel='time (s)', ylabel='voltage (mV)')
ax[0].grid()
ax[1].set(xlabel='time (s)', ylabel='voltage (mV)')
ax[1].grid()
ax[2].set(xlabel='time (s)', ylabel='voltage (mV)')
ax[2].grid()

fig.savefig("Ex5.png")
plt.show()
