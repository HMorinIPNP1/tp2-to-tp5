import matplotlib
import matplotlib.pyplot as plt
import numpy as np

T=1/100
t = np.arange(-5.0, 5.0, T)
s = np.exp(-1*abs(t))

fourier = np.fft.fft(s)/100
n = s.size
freq = np.fft.fftfreq(n, T)/10

fig, ax = plt.subplots(2)
ax[0].plot(t,s)
ax[1].plot(freq, abs(fourier.real), label="real")
ax[1].plot(freq, fourier.imag, label="imag")
ax[0].legend()
ax[1].legend()
plt.show()
fig.savefig("Ex8.png")
