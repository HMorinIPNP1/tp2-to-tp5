import matplotlib
import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0.0, 2.0, 0.01)
s = np.sin(2 * np.pi * t) + 0.5
b = np.sin(2 * np.pi * t) + np.random.random_sample(200)

fig, ax = plt.subplots()
ax.plot(t, s)
ax.plot(t, b)

ax.set(xlabel='time (s)', ylabel='voltage (mV)', title='About as simple as it gets')
ax.grid()

fig.savefig("Ex4.png")
plt.show()
