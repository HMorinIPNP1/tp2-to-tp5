import cv2
import matplotlib.pyplot as plt

name="paysage"
ext=".jpg"
image = cv2.imread(name+ext,0)
fig, ax = plt.subplots()
ax.hist(image.ravel(), bins = 256, cumulative = True)
#ax.set_xlabel('Intensity Value')
#ax.set_ylabel('Count') 
fig.savefig("Ex1c.png")
plt.show()