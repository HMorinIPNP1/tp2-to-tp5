from matplotlib import pyplot as plt
import cv2

name="paysage"
ext=".jpg"
img = cv2.imread(name+ext,0) 

fig, ax = plt.subplots()
ax.hist(img.ravel(),256,[0,256])
fig.savefig("Ex1b.png")
plt.show()