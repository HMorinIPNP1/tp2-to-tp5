import cv2
import matplotlib.pyplot as plt

name="paysage"
ext=".jpg"
img = cv2.imread(name+ext)
B=[]
G=[]
R=[]
largeur = img[0].size/3
largeur = int(largeur)
longueur = img.size/3
longueur = longueur/largeur
longueur = int(longueur)
for k in range (0,longueur):
	for j in range (0,largeur):
		for i in range (0,3):
			B.append(img[k,j,0])
			G.append(img[k,j,1])
			R.append(img[k,j,2])
fig, ax = plt.subplots(3)
ax[0].hist(B, bins = 256, color="blue", cumulative=True)
ax[0].set(title='BLUE')
#ax[0].xlabel('Intensity Value')
#ax[0].ylabel('Count') 
ax[1].hist(G, bins = 256, color="green", cumulative=True)
ax[1].set(title='GREEN')
#ax[1].xlabel('Intensity Value')
#ax[1].ylabel('Count') 
ax[2].hist(R, bins = 256, color="red", cumulative=True)
ax[2].set(title='RED')
#ax[2].xlabel('Intensity Value')
#ax[2S].ylabel('Count') 
fig.savefig("Ex1d.png")
plt.show()