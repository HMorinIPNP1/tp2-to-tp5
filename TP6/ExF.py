import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread("paysage.jpg")
kernel=cv2.getStructuringElement(cv2.MORPH_RECT,(15,15))
filt=cv2.morphologyEx(img,cv2.MORPH_CLOSE, kernel)

filt=cv2.cvtColor(filt, cv2.COLOR_BGR2RGB)
fig, ax= plt.subplots()
ax.imshow(filt)
fig.savefig("ExF.png")
ax.show()