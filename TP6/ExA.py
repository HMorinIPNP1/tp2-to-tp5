import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread("paysage.jpg")
kernel=np.array([[0,1,0], [1,1,1], [0,1,0]])
filt=cv2.filter2D(img,-1,kernel)

filt=cv2.cvtColor(filt, cv2.COLOR_BGR2RGB)
fig, ax= plt.subplots()
ax.imshow(filt)
fig.savefig("ExA.png")
ax.show()
