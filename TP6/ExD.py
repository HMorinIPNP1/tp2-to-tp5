import cv2
import matplotlib.pyplot as plt
import numpy as np

img = cv2.imread("paysage.jpg")
kernel=np.array([[32,64,32], [64,128,64], [32,64,32]])
filt=cv2.dilate(img,kernel,-1)

filt=cv2.cvtColor(filt, cv2.COLOR_BGR2RGB)
fig, ax= plt.subplots()
ax.imshow(filt)
fig.savefig("ExD.png")
ax.show()